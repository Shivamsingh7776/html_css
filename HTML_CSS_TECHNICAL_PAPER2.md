## CSS Responsive Queries:
Media queries are a popular technique for delivering a tailored style sheet to different devices. To demonstrate a simple example, we can change the background color for different devices:

![A](https://www.w3schools.com/css/mqcap.JPG)

**Example:-**
```css
/* Set the background color of body to tan */
body {
  background-color: tan;
}

/* On screens that are 992px or less, set the background color to blue */
@media screen and (max-width: 992px) {
  body {
    background-color: blue;
  }
}

/* On screens that are 600px or less, set the background color to olive */
@media screen and (max-width: 600px) {
  body {
    background-color: olive;
  }
}
```

## Media Queries For Menus:
In this example, we use media queries to create a responsive navigation menu, that varies in design on different screen sizes.

**Example:-**
```css
 /* The navbar container */
.topnav {
  overflow: hidden;
  background-color: #333;
}

/* Navbar links */
.topnav a {
  float: left;
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

/* On screens that are 600px wide or less, make the menu links stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .topnav a {
    float: none;
    width: 100%;
  }
} 
```
## Media Queries For Columns:
A common use of media queries, is to create a flexible layout. In this example, we create a layout that varies between four, two and full-width columns, depending on different screen sizes:
**Example:-**
```css
/* Create four equal columns that floats next to each other */
.column {
  float: left;
  width: 25%;
}

/* On screens that are 992px wide or less, go from four columns to two columns */
@media screen and (max-width: 992px) {
  .column {
    width: 50%;
  }
}

/* On screens that are 600px wide or less, make the columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
  }
}
```

**Example:-**
```css
/* Container for flexboxes */
.row {
  display: flex;
  flex-wrap: wrap;
}

/* Create four equal columns */
.column {
  flex: 25%;
  padding: 20px;
}

/* On screens that are 992px wide or less, go from four columns to two columns */
@media screen and (max-width: 992px) {
  .column {
    flex: 50%;
  }
}

/* On screens that are 600px wide or less, make the columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .row {
    flex-direction: column;
  }
}
```
## CSS Flexbox:
### CSS Flexbox Layout Module
Before the Flexbox Layout module, there were four layout modes:
* Block, for sections in a webpage.
* Inline, for text.
* Table, for two-dimensional table data.
* Positioned, for explicit position of an element.

The Flexible Box Layout Module, makes it easier to design flexible responsive layout structure without using float or positioning.
## Browser Support
The flexbox properties are supported in all modern browsers.
The element above represents a flex container (the blue area) with three flex items.
## Example:-
```css
 <div class="flex-container">
  <div>1</div>
  <div>2</div>
  <div>3</div>
</div> 
```
## CSS Grid Layout Module:
### Grid Layout:
The CSS Grid Layout Module offers a grid-based layout system, with rows and columns, making it easier to design web pages without having to use floats and positioning.

### Grid Elements:
A grid layout consists of a parent element, with one or more child elements.
**Example:-**
```css
 <div class="grid-container">
  <div class="grid-item">1</div>
  <div class="grid-item">2</div>
  <div class="grid-item">3</div>
  <div class="grid-item">4</div>
  <div class="grid-item">5</div>
  <div class="grid-item">6</div>
  <div class="grid-item">7</div>
  <div class="grid-item">8</div>
  <div class="grid-item">9</div>
</div> 
```
### Display Property:
An HTML element becomes a grid container when its display property is set to grid or inline-grid.

### Display Property:
An HTML element becomes a grid container when its display property is set to grid or inline-grid.
**Example:-**
```css
.grid-container {
  display: grid;
}
```
**Example:-**
```css
.grid-container {
  display: inline-grid;
}
```
All direct children of the grid container automatically become grid items.
### Grid Columns:
The vertical lines of grid items are called columns.

![A](https://www.w3schools.com/css/grid_columns.png)

### Grid Rows:
The horizontal lines of grid items are called rows.
![A](https://www.w3schools.com/css/grid_rows.png)

### Grid Gaps:
The horizontal lines of grid items are called rows.
![A](https://www.w3schools.com/css/grid_gaps.png)

**You can adjust the gap size by using one of the following properties:**
* column-gap
* row-gap
* gap

**Example:-**
```css
.grid-container {
  display: grid;
  column-gap: 50px;
}

```
**Example:-**
The row-gap property sets the gap between the rows:
```css
.grid-container {
  display: grid;
  row-gap: 50px;
}
```
**Example*:-*
The gap property is a shorthand property for the row-gap and the column-gap properties:
```css
.grid-container {
  display: grid;
  gap: 50px 100px;
}
```
## HTML <meta> Tag:
**Example:-**
Describe metadata within an HTML document:
```html
 <head>
  <meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML, CSS, JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head> 
```
### Definition and Usage:
* The <meta> tag defines metadata about an HTML document. Metadata is data (information) about data.
* <meta> tags always go inside the <head> element, and are typically used to specify character set, page description, keywords, author of the document, and viewport settings.
* Metadata will not be displayed on the page, but is machine parsable.

### Attributes:
* charset:-Specifies the character encoding for the HTML document 
* content:-text	Specifies the value associated with the http-equiv or name attribute.
* http-equiv:-Provides an HTTP header for the information/value of the content attribute.
* name:-Specifies a name for the metadata.
### Global Attributes:
The <meta> tag also supports the Global Attributes in HTML.

### Examples:-
Define keywords for search engines:
```html

<meta name="keywords" content="HTML, CSS, JavaScript">

```
Define a description of your web page:
```html

<meta name="description" content="Free Web tutorials for HTML and CSS">

```
Define the author of a page:
```html

<meta name="author" content="John Doe">

```
Refresh document every 30 seconds:
```html
<meta http-equiv="refresh" content="30">
```

### Setting the Viewport:
* The viewport is the user's visible area of a web page. It varies with the device - it will be smaller on a mobile phone than on a computer screen.
* You should include the following <meta> element in all your web pages:
```html

<meta name="viewport" content="width=device-width, initial-scale=1.0">

```



