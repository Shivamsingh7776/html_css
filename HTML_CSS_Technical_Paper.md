
# HTML/CSS Technical Paper:
##  Box Model:
In CSS, the term "box model" is used when talking about design and layout.

The CSS box model is essentially a box that wraps around every HTML element. It consists of: margins, borders, padding, and the actual content. The image below illustrates the box model:

![ALT](https://media.geeksforgeeks.org/wp-content/uploads/box-model-1.png)
* **Content** - - The content of the box, where text and images appear.
* **Padding** - Clears an area around the content. The padding is transparent.
* **Border** - A border that goes around the padding and content.
* **Margin** - Clears an area outside the border. The margin is transparent.

The box model allows us to add a border around elements, and to define space between elements. 
**Example**
```css
 div {

    width: 300px;
    border: 15px solid green;
    padding: 50px;
    margin: 20px;
}
```

## Width and Height of an Element
In order to set the width and height of an element correctly in all browsers, you need to know how the box model works.
**Example:-
This <div> element will have a total width of 350px: 
``` css

    width: 320px;
    padding: 10px;
    border: 5px solid gray;
    margin: 0; 

```
## Here is the calculation:
```
320px (width)
+ 20px (left + right padding)
+ 10px (left + right border)
+ 0px (left + right margin)
= 350px 
```

The total width of an element should be calculated like this:

Total element width = width + left padding + right padding + left border + right border + left margin + right margin

The total height of an element should be calculated like this:

Total element height = height + top padding + bottom padding + top border + bottom border + top margin + bottom margin.

## Inline versus Block Elements:
Every HTML element has a default display value, depending on what type of element it is.

There are two display values: block and inline.

### Block-level Elements:
* A block-level element always starts on a new line, and the browsers automatically add some space (a margin) before and after the element.
* A block-level element always takes up the full width available (stretches out to the left and right as far as it can).
* Two commonly used block elements are: <p> and <div>.
* The <p> element defines a paragraph in an HTML document.
* The <div> element defines a division or a section in an HTML document.
* ```The <p> element is a block-level element.```
* ``` The <div> element is a block-level element.```
**Example**:
```html
<p>Hello World</p>
<div>Hello World</div> 
```
### Here are the block-level elements in HTML:
```html

<address>
<article>
<aside>
<blockquote>
<canvas>
<dd>
<div>
<dl>
<dt>
<fieldset>
<figcaption>
<figure>
<footer>
<form>
<h1>-<h6>
<header>
<hr>
<li>
<main>
<nav>
<noscript>
<ol>
<p>
<pre>
<section>
<table>
<tfoot>
<ul>
<video>

```

### Inline Elements:
* An inline element does not start on a new line.
* An inline element only takes up as much width as necessary.
* This is ```a <span> element inside``` a paragraph.
```html
 <span>Hello World</span> 
```

### Here are the inline elements in HTML:
```html

<a>
<abbr>
<acronym>
<b>
<bdo>
<big>
<br>
<button>
<cite>
<code>
<dfn>
<em>
<i>
<img>
<input>
<kbd>
<label>
<map>
<object>
<output>
<q>
<samp>
<script>
<select>
<small>
<span>
<strong>
<sub>
<sup>
<textarea>
<time>
<tt>
<var>
```

**Note**: An inline element cannot contain a block-level element!
### The <span> Element:
* The <span> element is an inline container used to mark up a part of a text, or a part of a document.
* The <span> element has no required attributes, but style, class and id are common.
* When used together with CSS, the <span> element can be used to style parts of the text:

Example:
```html
<p>My mother has <span style="color:blue;font-weight:bold;">blue</span> eyes and my father has <span style="color:darkolivegreen;font-weight:bold;">dark green</span> eyes.</p>
```

## Positioning: Relative/Absolute:
The position property specifies the type of positioning method used for an element (static, relative, fixed, absolute or sticky).
### The position Property:
The position property specifies the type of positioning method used for an element.
* There are five different position values
1. static
2. relative
3. fixed
4. absolute
5. sticky

Elements are then positioned using the top, bottom, left, and right properties. However, these properties will not work unless the position property is set first. They also work differently depending on the position value.


#### position: static:
* HTML elements are positioned static by default.
* Static positioned elements are not affected by the top, bottom, left, and right properties.


* An element with position: static; is not positioned in any special way; it is always positioned according to the normal flow of the page
``` This <div> element has position: static;```
Example:-
``` css
div.static {
position: static;
border: 3px solid #73AD21;
}
```
#### position: relative:
* An element with position: relative; is positioned relative to its normal position.
* Setting the top, right, bottom, and left properties of a relatively-positioned element will cause it to be adjusted away from its normal position. Other content will not be adjusted to fit into any gap left by the element.
```  This <div> element has position: relative; ```
Example:-
```css
div.relative {
  position: relative;
  left: 30px;
  border: 3px solid #73AD21;
}
```

#### position: absolute:
* An element with position: absolute; is positioned relative to the nearest positioned ancestor (instead of positioned relative to the viewport, like fixed).However; if an absolute positioned element has no positioned ancestors, it uses the document body, and moves along with page scrolling.

**Example**
``` css
div.relative {
 position: relative;
width: 400px;
height: 200px;
border: 3px solid #73AD21;
}

div.absolute {
 position: absolute;
top: 80px;
right: 0;
width: 200px;
height: 100px;
border: 3px solid #73AD21;
}
```
## Common CSS structural classes:


### CSS structural classes:
Structural pseudo classes allow access to the child elements present within the hierarchy of parent elements. We can select first-child element, last-child element, alternate elements present within the hierarchy of parent elements.

The following is the list of structural classes.

#### 1. :first-child:
first-child represents the element that is prior to its siblings in a tree structure.

**Example**
```css
<style>
table tr:first-child{
 background-color:gray;
}
</style>
```
#### 2. :nth-child(n):
nth-child(Expression) class applies CSS properties to those elements that appear at the position evaluated by the resultant of an expression. The expression evaluates to a value resulting in the position of the element in a tree structure.

For example, :nth-child(2n+1) pseudo class applies to the rows of table that appear at the position of the given expression.

tr:nth-child(2n+1) represents rows such as 1th, 3th, 5th, 7th.... for the n values of 0, 1, 2, 3.......
**Example**
```css
<style>
table tr:nth-child(2n+1){
 background-color:gray;
}
</style>
```
#### 3. :last-child:
:last-child pseudo class represents the element that is at the end of its siblings in a tree structure.
**Example**
```css
<style>
ul li:last-child{
 background-color:lightblue;
}
</style>
```
It means the background-color of the last child of unordered list is lightblue.

#### 4. :nth-last-child(n):
nth-last-child(Expression) is the same as :nth-child(Expression) but the positioning of elements start from the end.

tr:nth-last-child(2n+1) represents last row, 3th last row, 5th last row, etc, element.

**Example:-**
``` css
<style>
table tr:nth-last-child(2n+1){
 background-color:lightblue;
}
</style>

```

nth-last-child(1) and :nth-last-child(2) means the last, 2nd last elements in the list of siblings.

#### 5. :only-child:
only-child represents the element that is a sole child of the parent element and there is no other sibling.
**Example:-**
```css
<style>
div p:only-child{
 background-color:lightblue;
}
</style>
```
It means the first and last elements are same.

#### 6. :first-of-type:
There might be more than one type of siblings under a common parent. It selects the first element of the one type of sibling.
**Example:-**
```css
<style>
dl dd:first-of-type{
 background-color:lightblue;
}
</style>
```

## Common CSS styling classes:
### What are Pseudo-classes?
A pseudo-class is used to define a special state of an element.

For example, it can be used to:

* Style an element when a user mouses over it.
* Style visited and unvisited links differently.
* Style an element when it gets focus.
#### Syntax

```css
selector:pseudo-class {
 property: value;
}
```
#### Anchor Pseudo-classes:
Links can be displayed in different ways:
**Example:-**
```css
 /* unvisited link */
a:link {
  color: #FF0000;
}

/* visited link */
a:visited {
  color: #00FF00;
}

/* mouse over link */
a:hover {
color: #FF00FF;
}

/* selected link */
a:active {
color: #0000FF;
} 
```

## CSS Specificity:
### What is Specificity?

If there are two or more CSS rules that point to the same element, the selector with the highest specificity value will "win", and its style declaration will be applied to that HTML element.

Think of specificity as a score/rank that determines which style declaration is ultimately applied to an element.

Look at the following examples:

### Example1:-

```html
 <html>
<head>
  <style>
    p {color: red;}
  </style>
</head>
<body>

<p>Hello World!</p>

</body>
</html> 
```
### Example2:-
```html
 <html>
<head>
  <style>
    .test {color: green;}
    p {color: red;}
  </style>
</head>
<body>

<p class="test">Hello World!</p>

</body>
</html> 
```
### Specificity Hierarchy :
Every CSS selector has its place in the specificity hierarchy.

There are four categories which define the specificity level of a selector:
* **Inline styles**:-Example:  ```css<h1 style="color: pink;">```
* **IDs** - Example: #navbar
* **Classes**, **pseudo-classes**, **attribute selectors** - Example: .test, :hover, [href]
* **Elements and pseudo-elements** - Example: h1, :before

## Reference:
1. https://www.w3schools.com/css/css_specificity.asp
2. https://www.geeksforgeeks.org/css-box-model/
3. https://www.web4college.com/css/web4-css-structural-classes.php
4. https://www.w3schools.com/css/css_positioning.asp
5. https://developer.mozilla.org/en-US/docs/Web/CSS/position

















